<?php

require_once(__DIR__ . '/../config/bootstrap.php');

use Classes\DataSource;
use Classes\Paginator;

// Ajax data
if (
    isset($_POST['ajaxData']) &&
    isset($_POST['ajaxCurrentUrl']) &&
    isset($_POST['ajaxDataSourceType'])
) {
    $data = json_decode($_POST['ajaxData'], true) ?? [];
    $currentPageUrl = $_POST['ajaxCurrentUrl'];
    $dataSourceType = $_POST['ajaxDataSourceType'];
    // Create a new Paginator
    $dataSource = new DataSource($dataSourceType);

    $paginator = new Paginator($dataSource);
    $paginator->setDataSourceData($data);
    $paginator->setCurrentPage($currentPageUrl);

    $response = [
        'next_page' => $paginator->getNextPage($currentPageUrl, false),
        'last_page' => $paginator->getNextPage($currentPageUrl, true),
        'current_page' => $paginator->getCurrentPage(),
        'previous_page' => $paginator->getPreviousPage($currentPageUrl),
        'get_page_1' => $paginator->getCurrentPage(1, $currentPageUrl, true),
        'get_page_2' => $paginator->getCurrentPage(2, $currentPageUrl, true),
        'get_page_current' => $paginator->getCurrentPage(0, $currentPageUrl, true),
        'total_pages' => $paginator->getTotalPages(),
        'total_rows' => $paginator->getTotalRows(),
        'total_items_per_row' => $paginator->getTotalItemsPerRow(),
        'total_items' => $paginator->getDataSourceDataSize(),
        'current_page_images_offset' => $paginator->getCurrentPageImageOffset($currentPageUrl),
        'dataSource_type' => $paginator->getDataSourceType(),
        'dataSource_data' => $paginator->getDataSourceData()
    ];

    echo json_encode($response);
}
