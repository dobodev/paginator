<?php

namespace Api;

use Classes\Interfaces\DataInterface;

class DefaultData implements DataInterface
{
    /** @var array<string|mixed> $data */
    private array $data;

    private int $dataSize;

    /**
     * Set data || generate default data & trigger to set dataSize
     * @param array<string|mixed> $data
     * @return void
     */
    public function setData(array $data = []): void
    {
        $this->data = $data;

        if (empty($data)) {
            $this->data = $this->generateDefaultData();
        }

        // Set dataSize
        $this->setDataSize($this->data);
    }

    /**
     * Get the data
     * @return array<string|mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set the data total size
     * @param array<string|mixed> $data
     * @return void
     */
    public function setDataSize(array $data): void
    {
        if (empty($data)) {
            $this->dataSize = sizeof($this->getData());
            return;
        }

        $this->dataSize = sizeof($data);
    }

    /**
     * Get the data total size
     * @return int
     */
    public function getDataSize(): int
    {
        return $this->dataSize;
    }

    /**
     * Generate default data
     * @return array<string|mixed>
     */
    private function generateDefaultData(): array
    {
        $data = [];
        $data = array_fill(0, 50, [
            'avatar' => $_ENV['APP_ASSETS_PATH'] . '/images/' . '/default.jpg',
            'first_name' => 'Dummy Name',
            'email' => 'dummy@example.com',
            'address' => [
                'country' => 'Dummy Country',
                'city' => 'Dummy City',
            ]
        ]);

        return $data;
    }
}
