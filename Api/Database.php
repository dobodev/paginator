<?php

namespace Api;

use Classes\Interfaces\DataInterface;

class Database implements DataInterface
{
    private string $driver;
    private string $host;
    private string $database_name;
    private string $port;
    private string $user;
    private string $password;
    private string $dsn;
    private \PDO $connection;

    /** @var array<bool|\PDO> $pdo_options*/
    private array $pdo_options;

    /** @var array<string|mixed> $data */
    private array $data;
    private int $dataSize;


    public function __construct()
    {
        // var_dump($_ENV);
        $this->driver = $_ENV['DB_CONNECTION_DRIVER']; // mysql
        $this->host = $_ENV['DB_HOST'];
        $this->database_name = $_ENV['DB_DATABASE'];
        $this->user = $_ENV['DB_USER'];
        $this->password = $_ENV['DB_PASSWORD'];
        $this->port = $_ENV['DB_PORT'];

        $this->dsn =
            $this->driver . ':dbname=' . $this->database_name . ';host=' .
            $this->host . ';port=' . $this->port . ';charset=utf8';

        $this->pdo_options = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        // Set the DB connection
        $this->setConnection();
    }

    /**
     * Set PDO connection
     * @return void
     */
    public function setConnection(): void
    {
        try {
            $this->connection = new \PDO($this->dsn, $this->user, $this->password, $this->pdo_options);
        } catch (\PDOException $e) {
            print "Database Connection Error: " . $e->getMessage();
            die();
        }
    }

    /**
      * Set data || generate default data & trigger to set dataSize
      * @param array<string|mixed> $data
      * @return void
     */
    public function setData(array $data = []): void
    {
        $this->data = $data;

        if (empty($data)) {
            $this->data = $this->generateDatabaseData();
        }

        // Set dataSize
        $this->setDataSize($this->data);
    }

    /**
     * Return database data
     * @return array<string|mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set dataSize
     * @param array<string|mixed> $data
     * @return int
     */
    public function setDataSize(array $data): int
    {
        if (empty($data)) {
            return $this->dataSize = sizeof($this->getData());
        }

        return $this->dataSize = sizeof($data);
    }

    /**
     * Get dataSize
     * @return int
     */
    public function getDataSize(): int
    {
        return $this->dataSize;
    }

     /**
     * Generate default data
     * @return array<string|mixed>
     */
    public function generateDatabaseData(): array
    {
        $data = [];
        $sqlQuery = "SELECT * FROM users";

        foreach ($this->connection->query($sqlQuery)->fetchAll() as $row) {
            $row['address'] = json_decode($row['address']);
            $row['avatar'] = $_ENV['APP_ASSETS_PATH'] . '/images/' . $row['avatar'];
            $data[] = $row;
        }

        return $data;
    }
}
