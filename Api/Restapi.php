<?php

namespace Api;

use Classes\Interfaces\DataInterface;

class Restapi implements DataInterface
{
    /** @var array<string|mixed> $data */
    private array $data;
    private int $dataSize;

     /**
      * Set data || generate default data & trigger to set dataSize
      * @param array<string|mixed> $data
      * @return void
     */
    public function setData(array $data = []): void
    {
        $this->data = $data;

        // Set dataSize
        $this->setDataSize($data);
    }

    /**
     * Get the data
     * @return array<string|mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set the data total size
     * @param array<string|mixed> $data
     * @return void
     */
    public function setDataSize(array $data): void
    {
        if (!$data) {
            $this->dataSize = sizeof($this->getData());
            return;
        }

        $this->dataSize = sizeof($data);
    }

   /**
     * Get the data total size
     * @return int
     */
    public function getDataSize(): int
    {
        return $this->dataSize;
    }
}
