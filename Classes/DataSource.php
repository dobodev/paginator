<?php

namespace Classes;

/**
 * class DataSource - creating instances of our data source classes
 */
class DataSource
{
    private mixed $source;

    public function __construct(string $source)
    {
        try {
            $source = ucfirst($source);
            $class = "Api\\{$source}";

            $this->source = new $class();
        } catch (\Throwable $th) {
            echo 'Fail to create DataSource object of type: ' . $source . ', ' . $th->getMessage();
        }
    }

    /**
     * Get our data source
     * @return mixed
     */
    public function getSource(): mixed
    {
        return $this->source;
    }
}
