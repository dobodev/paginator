<?php

namespace Classes;

/**
 * Base class - Singleton Pattern
 */
class Base
{
    private static Base $instance;

    /**
     * Construct - private visibility to avoid creating instances outside our class
     */
    private function __construct()
    {
        self::autoloadClasses();
    }

    /**
     * Checks if we got instance of the class created with public visibility
     * @return Base
     */
    public static function getInstance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new Base();
        }

        return self::$instance;
    }

    /**
     * Get our templates path directory
     * @param string $template
     * @return string
     */
    public function getTemplatesPath(string $template = ''): string
    {
        $templateDir = $_ENV['APP_TEMPLATES_PATH'];
        if ($template != '') {
            $templateFile = "{$template}.php";

            // Check if template exists
            if (file_exists("{$templateDir}/{$templateFile}")) {
                return "{$templateDir}/{$templateFile}";
            }

            return $templateDir;
        }

        return $templateDir;
    }

    /**
     * Get our assets path directory
     * @return string
     */
    public function getAssetsPath(): string
    {
        return $_ENV['APP_ASSETS_PATH'] . '/' . 'images';
    }

    /**
     * Get our home path
     * @return string
     */
    public function getHome(): string
    {
        return $_ENV['APP_HOME'];
    }

    /**
     * Autoload app classes
     * @return void
     */
    protected static function autoloadClasses(): void
    {
        spl_autoload_register(function ($class) {
            $rootPath = $_SERVER['DOCUMENT_ROOT'] . 'paginator/';
            $dirs = [
                $rootPath . 'classes/',
                $rootPath . 'api/',
                $rootPath . 'classes/interfaces/'
            ];

            foreach ($dirs as $dir) {
                if (file_exists($dir . $class . '.php')) {
                    require_once($dir . $class . '.php');
                } else {
                    $namespaceAsArray = explode('\\', $class);
                    $theClass = end($namespaceAsArray) . '.php';
                    if (file_exists($dir . $theClass)) {
                        require_once($dir . $theClass);
                    }
                }
            }
        });
    }
}
