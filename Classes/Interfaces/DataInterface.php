<?php

namespace Classes\Interfaces;

interface DataInterface
{
    /**
     * @return array<mixed>
    */
    public function getData();
}
