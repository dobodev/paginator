<?php

namespace Classes;

use Classes\DataSource;

class Paginator
{
    private int $items_per_page = 12; // ?dynamic | user input
    private int $items_per_row = 4; // ?dynamic | user input
    private ?int $current_page;
    private mixed $dataSource;

    public function __construct(DataSource $dataSource)
    {
        $this->setDataSource($dataSource);
    }

    /**
     * Set Datasource $dataSource
     * @param DataSource $dataSource
     * @return void
     */
    public function setDataSource(DataSource $dataSource): void
    {
        $this->dataSource = $dataSource;
    }

    /**
     * Set dataSource data
     * @param array<string|mixed> $data
     * @return void
     */
    public function setDataSourceData(array $data): void
    {
        $this->dataSource->getSource()->setData($data);
    }

    /**
     * Get data from the initialized DataSource
     * @return array<string|mixed>
     */
    public function getDataSourceData(): array
    {
        return $this->dataSource->getSource()->getData();
    }

    /**
     * Get Initialized DataSource [] size
     * @return int
     */
    public function getDataSourceDataSize(): int
    {
        return $this->dataSource->getSource()->getDataSize();
    }

    /**
     * Get Initialized DataSource class name as string
     *
     * @return string
     */
    public function getDataSourceType(): string
    {
        return get_class($this->dataSource->getSource());
    }

    /**
     * Get previous page class
     * @return string
     */
    public function getPreviousPageClass(): string
    {
        return $this->getCurrentPage() <= 1 ? 'disabled' : '';
    }

    /**
     * Get next page class
     * @return string
     */
    public function getNextPageClass(): string
    {
        return $this->getCurrentPage() >= $this->getTotalPages() ? "disabled" : "";
    }

    /**
     * Get last page class
     * @return string
     */
    public function getLastPageClass(): string
    {
        return $this->getCurrentPage() >= $this->getTotalPages() ? "link-danger" : "";
    }

    /**
     * Get total pages based on the items loaded
     * @return int
    */
    public function getTotalPages(): int
    {
        return (int) ceil($this->getDataSourceDataSize() / $this->items_per_page);
    }

    /**
     *
     * Get total rows per page
     * @return float
     */
    public function getTotalRows(): float
    {
        return ceil($this->items_per_page / $this->items_per_row);
    }

    /**
     * Get items per row
     * @return int
     */
    public function getTotalItemsPerRow(): int
    {
        return $this->items_per_row;
    }

    /**
     * Set current page based on the http request parameters
     * @param string $currentPageUrl
     * @return int|null
     */
    public function setCurrentPage(string $currentPageUrl): ?int
    {
        $parsedUrlArr = parse_url($currentPageUrl);

        $legalQueryParameters = $this->sanitizeGetParams($parsedUrlArr);
        // Unvalid one|many http query parameters
        if (!$legalQueryParameters) {
            return $this->current_page = null;
        }

        $this->current_page =  $legalQueryParameters['page'] ?? 1; // default page if missing http query param

        if ($this->current_page >= $this->getTotalPages()) {
            $this->current_page = $this->getTotalPages(); // max page value
        } elseif ($this->current_page <= 0) {
            $this->current_page =  1; // min page value
        }

        return $this->current_page;
    }

    /**
     * Get current | min/max page
     * @param int $exactPageNum - whether to set exact page
     * @param string $currentPageUrl
     * @param bool $parseAsUrl - whether to parse
     * @return int|string|null
     */
    public function getCurrentPage(
        int $exactPageNum = 0,
        string $currentPageUrl = '',
        bool $parseAsUrl = false
    ): int|string|null {
        if (is_null($this->current_page)) {
            return null;
        }

        if ($parseAsUrl && strlen($currentPageUrl) > 0) {
            $parsedUrlArr = parse_url($currentPageUrl);
            parse_str($parsedUrlArr['query'], $httpQuery);

            if ($exactPageNum > 0) {
                $httpQuery['page'] = $exactPageNum;
            } else {
                //  TODO fix logic for $pageNow
                // $pageNow = $httpQuery['page'] ?? $this->getCurrentPage();

                // if ($pageNow >= $this->getTotalPages()) {
                //     $httpQuery['page'] = $this->getTotalPages();
                // } elseif ($pageNow <= 1) {
                //     $httpQuery['page'] = 1;
                // } else {
                //     $httpQuery['page'] = $pageNow;
                // }
                $httpQuery['page'] = 1;
            }

            $parsedUrlArr['query'] = http_build_query($httpQuery);
            $pageAsUrl = $parsedUrlArr['path'] . '?' . $parsedUrlArr['query'];

            return $pageAsUrl;
        }

        return $this->current_page;
    }

     /**
     * Sanitize http query string
     * @param array<string|int|mixed> $parsedHttpUrl
     * @return array<null|mixed>
     */
    public function sanitizeGetParams(array $parsedHttpUrl): ?array
    {
        // Get the http-query url params as array
        parse_str($parsedHttpUrl['query'], $httpQuery);

        // List of allowed GET parameters to check against
        $allowedGetParams = ['page', 'dataSource'];
        $allowedGetParamsKeys = array_flip($allowedGetParams);

        $parameterSecured = [];

        foreach ($httpQuery as $parameter => $value) {
            // Unvalid http query param
            if (!array_key_exists($parameter, $allowedGetParamsKeys)) {
                return null;
            }

            # TODO: create Validation|Sanitization class to handle the process, with error msg to display ...
            // Sanitize
            $parameterSanitized[$parameter] = trim(htmlspecialchars($httpQuery[$parameter]));

            // Validate
            $parameterValidated[$parameter] = $this->validateGetParams($parameterSanitized);

            // Unvalid http query value
            if ($parameterValidated[$parameter] === false) {
                return null;
            }

            $parameterSecured[$parameter] = $parameterValidated[$parameter];
        }

        return $parameterSecured;
    }

    /**
     * Validate GET input
     * @param array<string> $parameter
     * @return float|string|bool
     */
    public function validateGetParams(array $parameter): string|float|bool
    {
        $validatedParameters = [];

        // Validate http query params
        foreach ($parameter as $key => $value) {
            if ($key == 'page') {
                $validatedParameters = filter_var($value, FILTER_VALIDATE_FLOAT);
            } elseif ($key == 'dataSource') {
                $options = ['restapi', 'database', 'defaultData'];

                $validatedParameters = $value;
                if (!in_array($value, $options)) {
                    $validatedParameters = false;
                }
            }
        }

        return $validatedParameters;
    }

    /**
     * Get current images offset, based on the get page parameter
     * @param string $currentPageUrl
     * @return float|int|null
     */
    public function getCurrentPageImageOffset(string $currentPageUrl = ''): float|int|null
    {

        if (is_null($this->getCurrentPage())) {
            return null;
        }

        //Remove Not needed, keeping for the index.php currently
        if (strlen($currentPageUrl) === 0) {
            return $offset = (($this->getCurrentPage() * $this->items_per_page) - $this->items_per_page);
        } else {
            $pageNow = $this->getCurrentPage();

            // Currently allowing -+ values over our real pages to be used => set min|max page based on the below
            $offset = ( ($pageNow * $this->items_per_page) - $this->items_per_page);
            if ($pageNow <= 0) {
                $offset = 0; // first item to start
            } elseif ($pageNow > $this->getTotalPages()) {
                $offset = ($this->getTotalPages() * $this->items_per_page) - $this->items_per_page;
            }

            return $offset;
        }
    }

    /**
     * Get previous page
     * @param string $currentPageUrl
     * @return string|null
     */
    public function getPreviousPage(string $currentPageUrl): ?string
    {
        if (is_null($this->getCurrentPage())) {
            return null;
        }

        // Get the [query] from the current page url Send
        $parsedUrlArr = parse_url($currentPageUrl);
        parse_str($parsedUrlArr['query'], $httpQuery);

        $pageNow = $this->getCurrentPage();
        if ($pageNow == 1) {
            return '#';
        } elseif ($pageNow >= $this->getTotalPages()) {
            $httpQuery['page'] = $this->getTotalPages() - 1;
        } else {
            $httpQuery['page'] = $pageNow - 1;
        }

        $parsedUrlArr['query'] = http_build_query($httpQuery);
        $previous_page = $parsedUrlArr['path'] . '?' . $parsedUrlArr['query'];

        return $previous_page;
    }

    /**
     * Get next page
     * @param string $currentPageUrl - http query string
     * @param bool $lastPage - true to return the last page,
     * @return string|null
     */
    public function getNextPage(string $currentPageUrl, bool $lastPage = false)
    {
        if (is_null($this->getCurrentPage())) {
            return null;
        }

        // Get the [query] from the current page url Send
        $parsedUrlArr = parse_url($currentPageUrl);
        parse_str($parsedUrlArr['query'], $httpQuery);

        $pageNow = $this->getCurrentPage();
        if ($pageNow == $this->getTotalPages()) {
            return '#';
        } elseif ($lastPage == true) {
            $httpQuery['page'] = $this->getTotalPages();
        } else {
            $httpQuery['page'] = $pageNow + 1;

            if ($pageNow >= $this->getTotalPages()) {
                $httpQuery['page'] = $this->getTotalPages();
            } elseif ($pageNow < 1) {
                $httpQuery['page'] = 1;
            }
        }

        $parsedUrlArr['query'] = http_build_query($httpQuery);
        $next_page = $parsedUrlArr['path'] . '?' . $parsedUrlArr['query'];

        return $next_page;
    }
}
