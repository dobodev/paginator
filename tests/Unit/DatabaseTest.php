<?php

namespace Tests\Unit;

use Api\Database;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class DatabaseTest extends TestCase
{
    private $databaseStub;
    public function setUp(): void
    {
        $this->databaseStub = $this->createStub(Database::class);
    }

    public function tearDown(): void
    {
        unset($this->databaseStub);
    }
    public function testGenerateDatabaseData()
    {
        $this->databaseStub
            ->method('generateDatabaseData')
            ->willReturn([
                'Jay', 'Smith', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}'
            ]);

        $result = $this->databaseStub->generateDatabaseData();

        $this->assertEquals(
            ['Jay', 'Smith', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}'],
            $result
        );
    }

    public function testGetData()
    {
        $this->databaseStub
            ->method('getData')
            ->willReturn([
                'Jay', 'Smith', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}'
            ]);

        $result = $this->databaseStub->getData();

        $this->assertEquals(
            ['Jay', 'Smith', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}'],
            $result
        );
    }

    #[DataProvider('dataEntry')]
    public function testSetDataSize(array $data)
    {
        $this->databaseStub
            ->method('setDataSize')
            ->willReturn(3);

        $result = $this->databaseStub->setDataSize($data);

        $this->assertEquals(3, $result);
    }


    /**
     * Data provider for the testSetDataSize method
     */
    public static function dataEntry()
    {
        return [
            [['Jay', 'Smith', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}']],
            [['John', 'Doe', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}']],
            [['Jane', 'Doe', '1984-01-01', 'jay@me.com', 'avatar.jpg', '{"city": "New York", "country": "USA"}']]
        ];
    }
}
