<?php

require __DIR__ . '/config/bootstrap.php';

use Classes\Base;

// Env variables for ajax requests
$apiHomeURL = $_ENV['API_HOME_URL'];
$randomDataApiURL = $_ENV['RANDOM_DATA_API_URL'];
$assetsPath = $_ENV['APP_ASSETS_PATH'];

/** @var Base $base */
$base = Base::getInstance();

// <!-- Header -->
include $base->getTemplatesPath('header');

// <!-- Content -->
include $base->getTemplatesPath('content');

// <!-- Footer -->
include $base->getTemplatesPath('footer');
